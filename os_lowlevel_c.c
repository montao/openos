#include <stdio.h>              /* Declaration of printf() */
#include "os_lowlevel_h.h"   /* Declarations of low-level stuff. */

/*
 * Declare system storage for thread-info.
 * 
 * The thread stack-pointer is saved and restored on a thread-switch.
 * 
 * The thread ID is managed by the thread creation and inspection routines,
 * but is otherwise generally ignored in the current os version.
 * 
 * The os_currently_running_thread variable is an index into the
 * os_thread_info_array. This array is compacted, so that all
 * positions from 0 up to (but not including) position number
 * os_current_thread_count is occupied. The index into the array may
 * have the same value as the thread ID, and then again it might not.
 */
#define   TASK_STACKSIZE       2048
/* Definition of Task Priorities */

#define TASK1_PRIORITY      1
#define TASK2_PRIORITY      2
#define MAX_THREADS ( 17 )
struct os_thread_info
{
  unsigned int * thread_sp; /* Storage space for thread stack-pointer. */
  int thread_id;            /* Storage space for a thread ID. */
};
struct os_thread_info os_thread_info_array[ MAX_THREADS ];
unsigned int os_currently_running_thread = 0;
unsigned int os_current_thread_count = 1;
unsigned int os_next_available_thread_id = 1;
/* Note: the thread-IDs will wrap around when an unsigned int
 * wraps around. There is no check for this, and there is no check
 * that a thread-ID really is unique if the IDs have wrapped around. */
 
/* Just to be helpful, we will keep track of how many thread-switches
 * are performed, so that we can print a correct count of them. */
unsigned int os_number_of_thread_switches = 0;

/*
 * os_internal_threadswitch - change thread
 * 
 * The thread stack-pointer is supplied as a parameter.
 * The old thread's stack-pointer value is saved to the array
 * os_thread_info_array, and a new thread is selected from the array.
 * The stack pointer of the new thread is returned.
 */
unsigned int * os_internal_threadswitch( unsigned int * old_sp )
{
  unsigned int * new_sp;

  os_number_of_thread_switches += 1; /* Increase thread-switch counter. */

  /* Print line 1 of an informational message. */
  printf( "\nPerforming thread-switch number %d. The system has been running for %d ticks.\n",
          os_number_of_thread_switches,
          os_get_internal_globaltime() );

  /* Save the stack pointer of the old thread. */
  os_thread_info_array[ os_currently_running_thread ].thread_sp = old_sp;

  /* Print part 1 of a message saying which threads are involved this time. */
  printf( "Switching from thread-ID %d ",
          os_thread_info_array[ os_currently_running_thread ].thread_id );

  /* Perform the scheduling decision (round-robin). */
  os_currently_running_thread += 1;
  if( os_currently_running_thread >= os_current_thread_count )
  {
    os_currently_running_thread = 0;
  }
  
  /* Print part 2 of the informational message. */
  printf( "to thread-ID %d.\n",
          os_thread_info_array[ os_currently_running_thread ].thread_id );
  
  /* Get the stack pointer of the new thread. */
  new_sp = os_thread_info_array[ os_currently_running_thread ].thread_sp;
  
  /* Return. */
  return( new_sp );
}

/*
 * os_create_thread
 * 
 * Thread creation, for a simple thread with statically allocated stack.
 * Argument 1: a pointer to function that the thread will run.
 *      If the function returns, we call os_exit_thread()
 * Argument 2: a pointer passed as an argument to the thread's function.
 *      The use of this argument is up to the writer of the thread's
 *      function. As an example, sometimes several threads will run the
 *      same code, but with different data or different starting values.
 *      For such a thread, the argument can point to the data or starting
 *      value to be used for the function. 
 * Argument 3: a pointer to the top of stack for the thread.
 *      The recommended stack size is at least 1024 bytes, since
 *      the stack must be large enough for all stack frames used by
 *      the thread's function, plus all functions called by that
 *      function - either directly or indirectly. Furthermore, the stack
 *      must at all times have room for 108 bytes of saved registers,
 *      in case the time-slice ends and a thread-switch occurs.
 * Return value: a thread ID, or -1 if there was a problem.
 */
int os_create_thread( int (*thread_function)(void *),
                         void * thread_arg,
                         unsigned int * thread_stack )
{
  int i;                /* Loop index. */
  int new_thread_id;    /* The thread-ID for the new thread. */
  
  /* new_thread_sp is used for the initial stack pointer of the new thread. */
  unsigned int * new_thread_sp;
  
  /* Set up the new thread's initial stack-frame.
   * The stack-frame is setup to look like the frame of a thread which
   * has been stopped by a thread-switch, after running for some time. 
   * The initial stack-frame will hold initial values for all registers.
   * 
   * Interrupts can be enabled while we fill the frame. There is no risk
   * that a partly-filled initial stack-frame could be used by the system,
   * since the new thread's stack-pointer value has not yet been written
   * into an os_thread_info structure in the os_thread_info_array.
   */
  new_thread_sp = thread_stack - 27;
  for( i = 1; i < 24; i += 1 ) new_thread_sp[ i ] = 0;
  new_thread_sp[ 24 ] = (unsigned int) os_internal_get_gp();
  new_thread_sp[ 25 ] = 0;
  new_thread_sp[ 26 ] = (unsigned int) os_exit_thread;
  new_thread_sp[  0 ] = (unsigned int) thread_function;
  new_thread_sp[  4 ] = (unsigned int) thread_arg;
  
  /*
   * Some explanation of the exception stack-frame
   * initialization code follows.
   * 
   * The global-pointer (gp) and stack-pointer (sp) must be setup
   * for the new thread. If you want to know what the gp is for, read
   * any book on compiler internals.
   * 
   * A thread corresponds to a C-function, and C-functions can exit
   * in a few ways: they can execute a return() statement, or they can
   * allow control (a fancy word for the program-counter) to reach
   * the end of the function (the closing brace).
   * In any of those cases, the function will execute the
   * "ret" assembly instruction, causing the contents of r31 to
   * be loaded into the program counter.
   * To make sure that something sensible happens in this case,
   * the slot for r31 in the stack-frame is pre-loaded with
   * the address of the os_exit_thread function.
   * This function will shut down the thread nicely, allowing
   * other threads to continue working.
   * 
   * Similarly, we pre-load the slot for r29 (the exception-return address)
   * with the address of the C-function containing the thread code.
   * The first time the thread runs, this address will be fetched from
   * the exception stack-frame and loaded into the program-counter.
   * This way, control will pass to the thread C-function.
   * Note: when the thread is interrupted, at the end of one of its
   * time-slices, the same slot in the exception stack-frame will hold the
   * program-counter value from the time of the interrupt.
   * 
   * The thread-argument is pre-loaded into the slot for r4. R4 is the
   * register for the first function-argument, so the thread C-function code
   * will be able to pick up the value just like any function picks up
   * any argument.
   */
  
  /* Now the real work starts. We must disable interrupts while
   * we update the system's internal data structures. */
  os_begin_critical_region();

  /* If the thread_info_array is full, a new thread cannot be created. */
  if( os_current_thread_count >= MAX_THREADS )
  {
    os_end_critical_region();
    return( -1 );
  }

  /* Allocate a thread ID and a thread_info_array entry. */
  new_thread_id = os_next_available_thread_id;
  os_next_available_thread_id += 1;
  os_thread_info_array[ os_current_thread_count ].thread_id = new_thread_id;
  os_thread_info_array[ os_current_thread_count ].thread_sp = new_thread_sp;
  os_current_thread_count += 1;
  
  /* Finished. Enable interrupts again and return. */
  os_end_critical_region();
  return( new_thread_id );
}

/*
 * os_exit_thread
 * 
 * Shut down a thread nicely, allowing other threads to continue,
 * and new threads to re-use some of the old thread's resources.
 * 
 * A thread can call this function directly, or it can be invoked indirectly
 * by the thread executing a "ret" assembly instruction. The latter case
 * is explained in the thread stack-frame initialization code.
 */
void os_exit_thread( void )
{
  int i;
  int current_thread_id;
  
  /* Disable interrupts while we modify the thread-management data-structures. */
  os_begin_critical_region();
  
  /* Save the current thread ID, in case someone checks for it. */
  current_thread_id = os_thread_info_array[ os_currently_running_thread ].thread_id;

  /* Shift array contents to compact ready queue.
   * When this code runs, it is the currently-running thread that has
   * requested its own removal from the system. We overwrite the
   * thread_info_array entry for this thread with the next one,
   * and so on, moving all the remaining entries down one slot. */
  for( i = os_currently_running_thread; i < os_current_thread_count - 1; i += 1 )
  {
    os_thread_info_array[ i ].thread_id = os_thread_info_array[ i + 1 ].thread_id;
    os_thread_info_array[ i ].thread_sp = os_thread_info_array[ i + 1 ].thread_sp;
  }
  /* We have now shifted all entries down one slot. The last thing to do
   * is to mark the last entry (the one with the highest index) as invalid.
   * This entry has just been copied down to the next-to-last slot, so
   * we're not losing it. */
  os_current_thread_count -= 1;
  
  /* Now set the currently-running thread to the entry that we just marked
   * as invalid. Later, we call os_yield, and then the thread stack-pointer
   * will be written to the invalid entry (and thereby thrown away). */
  os_currently_running_thread = os_current_thread_count;
  /* Set the thread ID correctly, in case someone checks for it. */
  os_thread_info_array[ os_currently_running_thread ].thread_id = current_thread_id;
  
  /* We're finished modifying the thread-management data-structures.
   * Enable interrupts again. */
  os_end_critical_region();
  
  /* In case the end-of-timeslice interrupt happens here, the currently-running
   * thread (the one which is exiting) will be thrown away - since its slot is
   * one-beyond the end of the valid part of the thread_info_array.
   * This is just what we want, of course.
   * In the other case, when the end-of-timeslice interrupt is not about to happen
   * immediately, we force a thread-switch by calling os_yield. This way,
   * the exiting thread will be thrown away without waiting for the timer interrupt
   * signalling the end of the timeslice. */
  os_yield();
}

/*
 * os_idle
 * 
 * This is the Idle thread. os_idle must be called once at the end of
 * the main() routine. os_idle never returns - it enters an infinite loop.
 * However, each iteration of the loop immediately yields the CPU so that other
 * threads can use the processor instead.
 */
void os_idle( void )
{
  while( 1 )            /* Loop forever. */
  {
    os_yield();      /* Of course we should yield here. But why? */ 
  //  Sleep(5000);        /* Should we call Sleep? Or perhaps, maybe not. */
  }
}   

/*
 * ********************************************************
 * *** You don't have to study the code below this line ***
 * ********************************************************
 */

/*
 * out_char_uart_0
 * 
 * Simple output routine, replaces putchar().
 * 
 * The integer argument corresponds to one
 * character, in the range 0 through 255.
 * This character is sent to uart_0.
 * Output is blocking (but a high-speed UART
 * will not block for very long).
 */
void out_char_uart_0( int c )
{
  /* Wait until transmitter is ready */
  while( (DE2_UART_0_BASE[2] & 0x40) == 0 );
  /* Now send character */
  DE2_UART_0_BASE[1] = c & 0xff;
}

/*
 * out_string_uart_0
 * 
 * Simple output routine, replaces printf()
 * for constant strings.
 * 
 * The argument is a pointer to an array of char.
 * The array can have any length, as long as there
 * is a trailing null-character at the end.
 * 
 * This routine calls out_char_uart_0 repeatedly,
 * to do the actual output.
 */
void out_string_uart_0( char * cp )
{
  while( *cp )
  {
    out_char_uart_0( *cp );
    cp += 1;
  }
}

/*
 * out_number_uart_0
 * 
 * Simple output routine, replaces printf()
 * for decimal numbers (%d conversion).
 * 
 * The integer argument is converted to a string
 * of digits representing the integer in decimal format.
 * The integer is considered signed, and a minus-sign
 * precedes the string of digits if the number is
 * negative. After conversion, the string is printed
 * using out_string_uart_0.
 * 
 * This routine will print a varying number of digits, from
 * one digit (for integers in the range 0 through 9) and up to
 * 10 digits and a leading minus-sign (for the largest negative
 * 32-bit integers).
 * 
 * If the integer has the special value
 * 100000...0 (that's 31 zeros), the number cannot be
 * negated. We check for this, and treat this as a special case.
 * If the integer has any other value, the sign is saved separately.
 * 
 * If the integer is negative, it is then converted to
 * its positive counterpart. We then use the positive
 * absolute value for conversion.
 * 
 * Conversion produces the least-significant digits first,
 * which is the reverse of the order in which we wish to
 * print the digits. We therefore store all digits in a buffer,
 * in ASCII form.
 * 
 * To avoid a separate step for reversing the contents of the buffer,
 * the buffer is initialized with an end-of-string marker at the
 * very end of the buffer. The digits produced by conversion are then
 * stored right-to-left in the buffer: starting with the position
 * immediately before the end-of-string marker and proceeding towards
 * the beginning of the buffer.
 * 
 * For this to work, the buffer size must of course be big enough
 * to hold the decimal representation of the largest possible integer,
 * and the minus sign, and the trailing end-of-string marker.
 * The value 24 for ITOA_BUFSIZ was selected to allow conversion of
 * 64-bit quantities; however, the size of an int on your current compiler
 * may not allow this straight away.
 */
#define ITOA_BUFSIZ ( 24 )
void out_number_uart_0( int num )
{
  int i, sign;
  static char itoa_buffer[ ITOA_BUFSIZ ];
  static const char maxneg[] = "-2147483648";
  
  itoa_buffer[ ITOA_BUFSIZ - 1 ] = 0;   /* Insert the end-of-string marker. */
  sign = num;                           /* Save sign. */
  if( num < 0 && num - 1 > 0 )          /* Check for most negative integer */
  {
    for( i = 0; i < sizeof( maxneg ); i += 1 )
    itoa_buffer[ i + 1 ] = maxneg[ i ];
    i = 0;
  }
  else
  {
    if( num < 0 ) num = -num;           /* Make number positive. */
    i = ITOA_BUFSIZ - 2;                /* Location for first ASCII digit. */
    do {
      itoa_buffer[ i ] = num % 10 + '0';/* Insert next digit. */
      num = num / 10;                   /* Remove digit from number. */
      i -= 1;                           /* Move index to next empty position. */
    } while( num > 0 );
    if( sign < 0 )
    {
      itoa_buffer[ i ] = '-';
      i -= 1;
    }
  }
  /* Since the loop always sets the index i to the next empty position,
   * we must add 1 in order to return a pointer to the first occupied position. */
  out_string_uart_0( &itoa_buffer[ i + 1 ] );
}

/*
 * os_hexasc
 * 
 * Supplementary routine for converting a 4-bit quantity
 * into an ASCII character representing the quantity in
 * hexadecimal form: 0 through 9, or A through F (for values
 * 10 and up).
 * 
 * This routine is used by out_hex_uart_0.
 */
int os_hexasc( int num )
{
  int tmp = num & 15;
  if( tmp > 9 ) tmp += ( 'A' - 10 );
  else tmp += '0';
  return( tmp );
}
/*
 * out_hex_uart_0
 * 
 * Simple output routine, replaces printf()
 * for hexadecimal numbers (%x conversion).
 * 
 * This routine performs an implicit conversion of the
 * input into an unsigned integer. A sign is never printed.
 * Negative inputs are shown as the hexadecimal representation
 * of their bit patterns (in unsigned form).
 * 
 * This routine always prints eight digits; if the input
 * value is small, leading zeroes will be printed.
 */
void out_hex_uart_0( int num )
{
  out_char_uart_0( os_hexasc( num >> 28 ) );
  out_char_uart_0( os_hexasc( num >> 24 ) );
  out_char_uart_0( os_hexasc( num >> 20 ) );
  out_char_uart_0( os_hexasc( num >> 16 ) );
  out_char_uart_0( os_hexasc( num >> 12 ) );
  out_char_uart_0( os_hexasc( num >>  8 ) );
  out_char_uart_0( os_hexasc( num >>  4 ) );
  out_char_uart_0( os_hexasc( num       ) );
}
