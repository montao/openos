/* File "os_lowlevel_h.h"
 * Copyright (c) 2008-2010 F Lundevall
 *   E-mail: flu at kth dot se
 * 
 *   This file is part of the os micro-operating system.
 * 
 *   os is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 3 of the License, or
 *   (at your option) any later version.
 * 
 *   os is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 * 
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * This file contains C-level declarations of some routines used by os.
 * 
 * File version: 1.8
 * os version: 1.8
 * Modification history for this file:
 * v1.8  (2010-11-15) Changed the stack pointer type, from unsigned int
 *                    to pointer-to-unsigned-int.
 * v1.7  (2008-07-07) Improved comments.
 * v1.6  (2008-07-04) Added os_idle, rearranged code, added comments.
 * v1.4  (2008-06-09) Added prototype for os_get_internal_tickcount.
 * v1.2  (2008-04-24) Added prototype for os_get_internal_globaltime.
 * v1.0  (2008-02-18) First released version.
 */

#ifndef os_LOWLEVEL_H_H_
#define os_LOWLEVEL_H_H_

/*
 * Address definitions.
 * These addresses are used by low-level routines.
 */
#define DE2_UART_0_BASE  ( (volatile unsigned int *) 0x860 )
#define DE2_TIMER_1_BASE ( (volatile unsigned int *) 0x920 )

/*
 * Function prototypes.
 * These functions are meant to be called from (user-written) code.
 */
int os_create_thread( int (*thread_function)(void *), void *, unsigned int * );
void os_exit_thread( void );
void os_begin_critical_region( void );
void os_end_critical_region( void );
void os_idle( void );
void os_yield( void );

/*
 * More function prototypes.
 * These functions return information about system data.
 */
void * os_internal_get_gp( void );
unsigned int os_get_internal_globaltime( void );
unsigned int os_get_internal_tickcount( void );

/*
 * The prototype for os_internal_threadswitch.
 * This function must never be called except from one place,
 * inside the exception service routine that switches threads.
 */
unsigned int * os_internal_threadswitch( unsigned int * );

/*
 * Yet more function prototypes.
 * These functions are helper functions for printf().
 * In most cases, you should call printf() rather than
 * calling one of these functions.
 */
void out_char_uart_0( int );
void out_hex_uart_0( int );
void out_string_uart_0( char * );
void out_number_uart_0( int );

#endif /*os_LOWLEVEL_H_H_*/
